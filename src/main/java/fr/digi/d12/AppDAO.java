package fr.digi.d12;

import fr.digi.d12.bo.User;
import fr.digi.d12.dal.DAOFactory;
import fr.digi.d12.dal.IUserDAO;

import java.util.Scanner;

public class AppDAO {
	
	private static final Scanner sc = new Scanner( System.in );
	
	public static void main( String[] args ) {
		authenticate();
	}
	
	private static void authenticate() {
		
		System.out.println( "Bienvenue à toi... Merci de te connecter :" );
		System.out.print( "Login : " );
		String login = sc.nextLine();
		System.out.print( "Mot de passe : " );
		String password = sc.nextLine();
		
		IUserDAO dao = DAOFactory.getUserDAO();
		try {
			User user = dao.authentication( login, password );
			System.out.printf( "Bienvenue %s%n", user.getLogin() );
		} catch ( Exception e ) {
			System.err.println( e.getMessage() );
		}
	}
}
