package fr.digi.d12;

import java.sql.*;
import java.util.ResourceBundle;

/**
 * Hello world!
 */
public class App {
	
	private static final String DB_URL;
	private static final String DB_LOGIN;
	private static final String DB_PWD;
	
	static {
		ResourceBundle props = ResourceBundle.getBundle( "db" );
		DB_URL = props.getString( "jdbc.db.url" );
		DB_LOGIN = props.getString( "jdbc.db.login" );
		DB_PWD = props.getString( "jdbc.db.pwd" );
	}
	
	public static void main( String[] args ) {
		creerArticle();
		listerArticle();
		
		// Connection connection = null;
		// try {
		// 	connection = DriverManager.getConnection( url, login, pwd );
		// 	System.out.println(connection);
		// } catch ( SQLException e ) {
		// 	System.out.println(e.getMessage());
		// } finally {
		// 	try {
		// 		connection.close();
		// 	} catch ( SQLException e ) {
		// 		System.out.println(e.getMessage());
		// 	}
		// }
	}
	
	private static void creerArticle() {
		try ( Connection connection = DriverManager.getConnection( DB_URL, DB_LOGIN, DB_PWD ); Statement st = connection
				.createStatement() ) {
			// connection.setAutoCommit( false );
			System.out.println( st
					.executeUpdate( "INSERT INTO article (REF, DESIGNATION) VALUES ('FOX34', 'Ecran de Gab')" ) );
			// connection.commit();
			// connection.rollback();
		} catch ( SQLException e ) {
			System.out.println( e.getMessage() );
		}
	}
	
	private static void listerArticle() {
		try ( Connection connection = DriverManager.getConnection( DB_URL, DB_LOGIN, DB_PWD ) ) {
			System.out.println( connection );
			try ( Statement st = connection.createStatement(); ResultSet rs = st
					.executeQuery( "SELECT * FROM article" ) ) {
				while ( rs.next() ) {
					System.out
							.println( rs.getString( "ID" ) + " - REF " + rs.getString( "REF" ) + " - DESIGNATION " + rs
									.getString( "DESIGNATION" ) );
				}
			}
		} catch ( SQLException e ) {
			System.out.println( e.getMessage() );
		}
	}
}
