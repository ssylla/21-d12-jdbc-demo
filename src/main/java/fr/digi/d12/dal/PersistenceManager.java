package fr.digi.d12.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public final class PersistenceManager {
	
	private static final String DB_URL;
	private static final String DB_LOGIN;
	private static final String DB_PWD;
	private static Connection connection;
	
	static {
		ResourceBundle props = ResourceBundle.getBundle( "db" );
		DB_URL = props.getString( "jdbc.db.url" );
		DB_LOGIN = props.getString( "jdbc.db.login" );
		DB_PWD = props.getString( "jdbc.db.pwd" );
	}
	
	private PersistenceManager() {}
	
	public static Connection getConnection() throws SQLException {
		
		if ( null == connection || connection.isClosed() ) {
			connection = DriverManager.getConnection( DB_URL, DB_LOGIN, DB_PWD );
		}
		return connection;
	}
	
	public static void closeConnection() throws SQLException {
		if ( null != connection && !connection.isClosed() ) {
			connection.close();
		}
	}
}
