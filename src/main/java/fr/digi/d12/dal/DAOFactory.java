package fr.digi.d12.dal;

import java.util.ResourceBundle;

public final class DAOFactory {
	
	private static final String STORE_MODE;
	
	static {
		ResourceBundle props = ResourceBundle.getBundle( "db" );
		STORE_MODE = props.getString( "store.mode" );
	}
	
	private DAOFactory() {}
	
	public static IUserDAO getUserDAO() {
		
		IUserDAO dao = null;
		switch ( STORE_MODE ) {
			case "JDBC":
				dao = new UserJDBCDAO();
				break;
		}
		return dao;
	}
}
