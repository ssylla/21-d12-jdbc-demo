package fr.digi.d12.dal;

import fr.digi.d12.bo.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class UserJDBCDAO implements IUserDAO {
	
	private static final String AUTH_QUERY = "SELECT * FROM utilisateur WHERE LOGIN=? AND MOT_DE_PASSE=?";
	
	@Override
	public void create( User user ) {
		//TODO
	}
	
	@Override
	public void update( User user ) {
		//TODO
	}
	
	@Override
	public List<User> findAll() {
		//TODO
		return null;
	}
	
	@Override
	public User findById( Integer id ) {
		//TODO
		return null;
	}
	
	@Override
	public User authentication( String login, String password ) throws Exception {
		User user = null;
		Connection connection = PersistenceManager.getConnection();
		try ( PreparedStatement pst = connection.prepareStatement( AUTH_QUERY ) ) {
			pst.setString( 1, login );
			pst.setString( 2, password );
			System.out.println( pst );
			try ( ResultSet rs = pst.executeQuery() ) {
				if ( rs.next() ) {
					user = new User( rs.getInt( 1 ), rs.getString( 2 ), rs.getString( 3 ) );
				} else {
					throw new Exception( "Utilisateur non trouvé !" );
				}
			}
		}
		return user;
	}
}
