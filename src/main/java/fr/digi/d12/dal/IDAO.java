package fr.digi.d12.dal;

import java.util.List;

public interface IDAO<T, ID> {
	
	void create( T object );
	
	void update( T object );
	
	List<T> findAll();
	
	T findById( ID id );
}
