package fr.digi.d12.dal;

import fr.digi.d12.bo.User;

public interface IUserDAO extends IDAO<User, Integer> {
	User authentication( String login, String password ) throws Exception;
}
